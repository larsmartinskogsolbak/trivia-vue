# trivia-vue

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

**Components**: 

**GameMenu**
A component for a start page where the user can input their name and start the game. 

**GamePlay**
A component that receives QuizQuestion to handle the active game. It fetches questions from the OpenTrivia API and has methods for handling loading the next question or ending the game when the user clicks the answer button in the QuizQuestion component.

**QuizQuestion**
A component that displays the fetched questions and buttons for answering them. It also shuffles the array of questions so that the questions are always different. 

**GameOver**
A component that displays the results of the game. It displays the score and has buttons to restart the game or go back to the GameMenu component. 

**QuizResults**
A component that handles the result of each answered question. It displays what the user answered and what the correct answer was. 