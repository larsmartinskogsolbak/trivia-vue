import Vue from "vue";
import VueRouter from "vue-router";
import App from "./App.vue";
import { BootstrapVue, IconsPlugin } from "bootstrap-vue";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";

import GameMenu from "./components/GameMenu.vue";
import GamePlay from "./containers/GamePlay.vue";
import GameOver from "./containers/GameOver.vue";

Vue.use(VueRouter);
Vue.use(BootstrapVue);
Vue.use(IconsPlugin);
Vue.config.productionTip = false;

const router = new VueRouter({
  mode: "history",
  base: "/",
  routes: [
    { path: "/", name: "GameMenu", component: GameMenu },
    { path: "/play/", name: "GamePlay", component: GamePlay },
    { path: "/over/", name: "GameOver", component: GameOver, props: true }
  ]
});

new Vue({
  router,
  render: h => h(App)
}).$mount("#app");
